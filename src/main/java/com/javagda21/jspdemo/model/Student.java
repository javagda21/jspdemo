package com.javagda21.jspdemo.model;

import lombok.Data;

@Data
public class Student {
    private static Long ID_COUNTER = 1L;

    private Long id;
    private String firstName;
    private String lastName;
    private int age;

    public Student() {
        this.id = ID_COUNTER++;
    }
}
