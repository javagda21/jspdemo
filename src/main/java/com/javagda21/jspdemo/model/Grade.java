package com.javagda21.jspdemo.model;

import lombok.Data;

@Data
public class Grade {
    private Double value;
    private Subject subject;
    private boolean correctable;
}
