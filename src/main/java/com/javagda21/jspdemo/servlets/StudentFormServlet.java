package com.javagda21.jspdemo.servlets;

import com.javagda21.jspdemo.model.Student;
import com.javagda21.jspdemo.session.StudentSessionDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// strona formularza
@WebServlet("/student/add")
public class StudentFormServlet extends HttpServlet {
    private StudentSessionDao dao = new StudentSessionDao();

    // get - wyświetl mi formularz
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // wyświetl formularz
        req.getRequestDispatcher("/student/form.jsp").forward(req, resp);
    }

    // post - wyślij treść formularza na serwer
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student student = new Student();
        student.setFirstName(req.getParameter("name"));
        student.setLastName(req.getParameter("surname"));
        student.setAge(Integer.parseInt(req.getParameter("age")));

        dao.save(student, req.getSession());
        // nie wpisujemy jsp
        resp.sendRedirect("/student/list");
    }
}
