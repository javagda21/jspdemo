package com.javagda21.jspdemo.servlets;

import com.javagda21.jspdemo.session.StudentSessionDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/student/list")
public class StudentListServlet extends HttpServlet {
    private StudentSessionDao dao = new StudentSessionDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // wyświetl formularz
        req.setAttribute("studentList", dao.pobierzZSesjiStudentow(req.getSession()));

        req.getRequestDispatcher("/student/list.jsp").forward(req, resp);
    }
}
