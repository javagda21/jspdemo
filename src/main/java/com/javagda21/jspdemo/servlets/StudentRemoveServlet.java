package com.javagda21.jspdemo.servlets;

import com.javagda21.jspdemo.session.StudentSessionDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/student/remove")
public class StudentRemoveServlet extends HttpServlet {
    private StudentSessionDao dao = new StudentSessionDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int position = Integer.parseInt(req.getParameter("position"));
        // pozycja studenta którego należy usunąć.

        dao.removeStudentAtPosition(req.getSession(), position); // usun studenta na pozycji 'position'

        resp.sendRedirect("/student/list");
    }
}
