package com.javagda21.jspdemo.servlets;

import com.javagda21.jspdemo.model.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// adres:port/index (GET) (POST)

// dwie niezbędne rzeczy (@WebServlet, HttpServlet) żeby klasa była servletem
@WebServlet("/index") // adres pod którym aktywowany będzie servlet
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // wewnątrz metody getRequestDispatcher wpisujemy adres pliku "jsp" (widoku)
        // request Dispatcher ładuje template'y
        //
        //
        req.getRequestDispatcher("/indexHello.jsp").forward(req, resp);
    }

}
