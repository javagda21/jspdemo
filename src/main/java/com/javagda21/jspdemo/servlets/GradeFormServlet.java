package com.javagda21.jspdemo.servlets;

import com.javagda21.jspdemo.model.Grade;
import com.javagda21.jspdemo.model.Student;
import com.javagda21.jspdemo.model.Subject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/grade/add")
public class GradeFormServlet extends HttpServlet {
    public static List<Grade> grades = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/grade/form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String value = req.getParameter("value"); //<input type="number" step="0.5" min="2" max="5.5" name="value">
        String correctable = req.getParameter("correctable"); //<input type="checkbox" name="correctable">
        String subject = req.getParameter("subject"); //<input type="text" name="subject">

        boolean checked = (correctable != null) && correctable.equalsIgnoreCase("on");
        Subject subjectEnum = Subject.valueOf(subject.toUpperCase());
        Double valueDouble = Double.parseDouble(value);

        Grade grade = new Grade();
        grade.setCorrectable(checked);
        grade.setSubject(subjectEnum);
        grade.setValue(valueDouble);

        grades.add(grade);
        resp.sendRedirect("/grade/list"); //przekierowanie
    }
}
