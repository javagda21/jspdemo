package com.javagda21.jspdemo.session;

import com.javagda21.jspdemo.model.Student;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class StudentSessionDao {

    // SERWER  -------------------  SESJA ------------------------   KLIENT
    private static final String STUDENT_LIST_ATTRIBUTE = "STUDENT_LIST_ATTR";

    public void save(Student student, HttpSession session) {
        List<Student> students = pobierzZSesjiStudentow(session); // pobierz

        students.add(student); // dodaj

        zapiszStudentowWSesji(students, session); // zapisz
    }

    public List<Student> pobierzZSesjiStudentow(HttpSession session){
        List<Student> list;
        if(session.getAttribute(STUDENT_LIST_ATTRIBUTE) != null){
            list = (List<Student>) session.getAttribute(STUDENT_LIST_ATTRIBUTE);
        }else{
            list = new ArrayList<>();
        }
        return list;
    }

    /**
     * Żeby zapisać studentów w sesji, umieszczamy ich jako atrybut pod wcześniej
     * zdefiniowanym kluczem.
     * @param list - lista do zapisania.
     * @param session - sesja.
     */
    private void zapiszStudentowWSesji(List<Student> list, HttpSession session){
        session.setAttribute(STUDENT_LIST_ATTRIBUTE, list);
    }

    public void removeStudentAtPosition(HttpSession session, int position) {
        List<Student> students = pobierzZSesjiStudentow(session); // pobierz

        students.remove(position); // usun na pozycji

        zapiszStudentowWSesji(students, session); // zapisz
    }
}
