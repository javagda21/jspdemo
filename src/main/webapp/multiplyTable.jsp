<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/19
  Time: 9:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnozenia</title>
</head>
<body>

<div>
    <%--action = URL który ma się wywołać --%>
    <%--Gdzie chcemy wysłać informacje z formularza--%>
    <form action="/multiplyTable.jsp" method="get">
        <input type="number" name="nazwa_zmiennejX">
        </br> <%--Oddzielam linię--%>
        <input type="number" name="nazwa_zmiennejY">
        </br> <%--Oddzielam linię--%>

        <%--Guzik zatwierdzający wysłanie formularza--%>
        <input type="submit" value="mnozonko">
    </form>
</div>

<hr>
<table>
    <%
        //        rozpoczęcie bloku kodu java
            // adres:port/multiplyTable.jsp?nazwa_zmiennejX=5&nazwa_zmiennejY=10
        String paramX = request.getParameter("nazwa_zmiennejX");
        String paramY = request.getParameter("nazwa_zmiennejY");

        int wielkoscTabliczkiX = 10;
        int wielkoscTabliczkiY = 10;

        if ((paramX == null && paramY == null) || (paramX.isEmpty() && paramY.isEmpty())) {
//             po prostu 10 x 10
        } else if (paramX == null || paramY == null || paramX.isEmpty() || paramY.isEmpty()) {
            if (paramX == null || paramX.isEmpty()) {
                try {
                    wielkoscTabliczkiX = Integer.parseInt(paramY);
                } catch (NumberFormatException nfe) {
                }
            }
            if (paramY == null || paramY.isEmpty()) {
                try {
                    wielkoscTabliczkiX = Integer.parseInt(paramX);
                } catch (NumberFormatException nfe) {
                }
            }
            wielkoscTabliczkiY = wielkoscTabliczkiX;
        } else {
            try {
                wielkoscTabliczkiX = Integer.parseInt(paramX);
            } catch (NumberFormatException nfe) {
            }

            try {
                wielkoscTabliczkiY = Integer.parseInt(paramY);
            } catch (NumberFormatException nfe) {
            }
        }


        for (int i = 0; i < wielkoscTabliczkiX; i++) {  // wierszami
            out.print("<tr>");          // rozpoczęcie wiersza.
            for (int j = 0; j < wielkoscTabliczkiY; j++) { // kolumny
                out.println("<td>" + ((i + 1) * (j + 1)) + "</td>"); // komórka
            }
            out.println("</tr>");
        }
    %>
</table>
</body>
</html>
