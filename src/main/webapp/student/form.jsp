<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/19
  Time: 11:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Form</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<%-- Formularz dodawania studenta. --%>
<form action="/student/add" method="post">
    <input type="text" name="name"><br>
    <input type="text" name="surname"><br>
    <input type="number" name="age" min="1" max="100"><br>
    <input type="submit">
</form>
</body>
</html>
