<%@ page import="com.javagda21.jspdemo.model.Student" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/19
  Time: 12:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student list</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>
<table>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Age</th>
        <th>Remove</th>
    </tr>
    <%--Nagłówek--%>
    <%
        List<Student> students = (List<Student>) request.getAttribute("studentList");
        for (int i = 0; i < students.size(); i++) {
            Student s = students.get(i);

            out.print("<tr>");
            out.print("<td>" + s.getFirstName() + "</td>");
            out.print("<td>" + s.getLastName() + "</td>");
            out.print("<td>" + s.getAge() + "</td>");
            out.print("<td>" +
                    "<a href=\"/student/remove?position=" + i + "\">Remove</a>" +
                    "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
