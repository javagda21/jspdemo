<%@ page import="com.javagda21.jspdemo.model.Subject" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/19
  Time: 2:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Grade Form</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<%-- Formularz dodawania oceny. --%>
<form action="/grade/add" method="post">
    Ocena: <input type="number" step="0.5" min="2" max="5.5" name="value"><br>
    <%--Przedmiot: <input type="text" name="subject"><br>--%>
    <select name="subject">
        <%
            //Subject.values() - to wszystkie opcje enum'a
            for (Subject s : Subject.values()) {
        %>
              <option value="<%= s %>"><%= s %></option>
        <%
            }
        %>
    </select><br>
    Czy można poprawić: <input type="checkbox" name="correctable"><br>
    <input type="submit">
</form>
</body>
</html>
