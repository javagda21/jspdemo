<%@ page import="com.javagda21.jspdemo.model.Grade" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 7/20/19
  Time: 2:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>
<table>
    <tr>
        <th>Value</th>
        <th>Subject</th>
        <th>Correctable</th>
    </tr>
    <%--Nagłówek--%>
    <%
        List<Grade> grades = (List<Grade>) request.getAttribute("gradeList");
        for (Grade s : grades) {
            out.print("<tr>");
            out.print("<td>" + s.getValue() + "</td>");
            out.print("<td>" + s.getSubject() + "</td>");
            out.print("<td>" + s.isCorrectable() + "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
